import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import Home from "@/views/Home.vue";

describe("Home.vue", () => {
  it("check output ", () => {
    const wrapper = shallowMount(Home)
    // wrapper.find(videos)
    expect(wrapper.vm.videos).toContain('title')
  });
});
