import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    videos: []
  },
  mutations: {
    init (state) {
      // mutate state
      state.videos = []
    }
  },
  actions: {},
  modules: {}
});
