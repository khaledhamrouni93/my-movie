import axios from 'axios'

const Url = 'https://api.ocs.fr/apps/v2/contents?search=title='
export const BaseUrl = 'https://api.ocs.fr'

export const GetData = async (params) => {
    try {
        const response = await axios.get(Url + params)
    return response.data
    } catch (error) {
        if (error.response) {
            console.log(error.response.data)
            console.log(error.response.status)
        } else if (error.request) {
            console.log(error.request)
        } else {
            console.log('Error', error.message)
        }
        console.log(error)
    }
    
}

export const GetDetail = async (params) => {
    try {
        const response = await axios.get(BaseUrl + params)
    return response.data
    } catch (error) {
        if (error.response) {
            console.log(error.response.data)
            console.log(error.response.status)
        } else if (error.request) {
            console.log(error.request)
        } else {
            console.log('Error', error.message)
        }
        console.log(error)
    }
    
}

